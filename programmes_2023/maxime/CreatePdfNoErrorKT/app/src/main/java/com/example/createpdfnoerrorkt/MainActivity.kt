package com.example.createpdfnoerrorkt


import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
//import com.danjdt.pdfviewer.PdfViewer
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.StandardCopyOption


class MainActivity : AppCompatActivity() {

    lateinit var edittext:EditText // pour le texte a l'interieur du pdf
    lateinit var editdossier: EditText // pour le nom du fichier
    lateinit var generatePDFBtn: Button

    var pageHeight = 1120
    var pageWidth = 792

    // creating a bitmap variable
    // for storing our images
    lateinit var bmp: Bitmap
    lateinit var scaledbmp: Bitmap

    // on below line we are creating a
    // constant code for runtime permissions.
    var PERMISSION_CODE = 101




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttonSelectFile = findViewById<Button>(R.id.button_select_file)
        buttonSelectFile.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                type = "*/*"
                addCategory(Intent.CATEGORY_OPENABLE)
            }
            filePickerLauncher.launch(Intent.createChooser(intent, "Select a File"))
        }
        /*val spinner = findViewById<Spinner>(R.id.spinner2)
        val arrayAdpter = ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,players)
        spinner.adapter = arrayAdpter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        override fun onItemselected (
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long

        ) {
            Toast.makeText(applicationContext,"le joueur selectionné est = " + players[position],Toast.LENGTH_SHORT).show()
        }*/



        edittext=findViewById(R.id.donnees)
        editdossier=findViewById(R.id.nomdossier)
        generatePDFBtn = findViewById(R.id.idBtnGeneratePdf)

        // on below line we are initializing our bitmap and scaled bitmap.
        bmp = BitmapFactory.decodeResource(resources, R.drawable.android)

        scaledbmp = Bitmap.createScaledBitmap(bmp, 140, 140, false)

        // on below line we are checking permission
      //  if (checkPermissions()) {
            // if permission is granted we are displaying a toast message.
          //  Toast.makeText(this, "Permission accordée", Toast.LENGTH_SHORT).show()
      //  } else {
            // if the permission is not granted
            // we are calling request permission method.
        //    requestPermission()
       // }

        // on below line we are adding on click listener for our generate button.
        generatePDFBtn.setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                // on below line we are calling generate
                // PDF method to generate our PDF file.
                generatePDF()
            }
        }
    }

    // on below line we are creating a generate PDF method
    // which is use to generate our PDF file.
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun generatePDF() {

        var pdfDocument: PdfDocument = PdfDocument()
        val Donnees = edittext.text.toString()
        val nomdossier = editdossier.text.toString()
        var nomdossierpdf = nomdossier + ".pdf"
        var paint: Paint = Paint()
        var title: Paint = Paint()
        var myPageInfo: PdfDocument.PageInfo? =
            PdfDocument.PageInfo.Builder(pageWidth, pageHeight, 1).create()
        var myPage: PdfDocument.Page = pdfDocument.startPage(myPageInfo)
        var canvas: Canvas = myPage.canvas


        canvas.drawBitmap(scaledbmp, 56F, 40F, paint)
        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
        title.textSize = 15F
        title.setColor(ContextCompat.getColor(this, R.color.purple_200))
        canvas.drawText("Test Maxime Bousseau ", 209F, 80F, title)
        title.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL))
        title.setColor(ContextCompat.getColor(this, R.color.purple_200))
        title.textSize = 15F


        title.textAlign = Paint.Align.CENTER
        canvas.drawText(Donnees, 396F, 560F, title)


        pdfDocument.finishPage(myPage)
        val file: File = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), nomdossierpdf)

        try {
            pdfDocument.writeTo(FileOutputStream(file))

            Toast.makeText(applicationContext, "Fichier Pdf crée", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {

            e.printStackTrace()

            Toast.makeText(applicationContext, "Erreur lors de la création du fichier PDF", Toast.LENGTH_SHORT)
                .show()
        }

        pdfDocument.close()
    }
    var filePickerLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == AppCompatActivity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val uri: Uri = data.data!!
                val inputStream: InputStream? = contentResolver.openInputStream(uri)
                val fileName: String? = getFileName(uri)
                Toast.makeText(this, "Le fichier selectionné est : $fileName", Toast.LENGTH_SHORT).show()
                // Faites quelque chose avec le fichier sélectionné ici
            }
        }
    }
    fun checkPermissions(): Boolean {

        var writeStoragePermission = ContextCompat.checkSelfPermission(
            applicationContext,
            WRITE_EXTERNAL_STORAGE
        )


        var readStoragePermission = ContextCompat.checkSelfPermission(
            applicationContext,
            READ_EXTERNAL_STORAGE
        )


        return writeStoragePermission == PackageManager.PERMISSION_GRANTED
                && readStoragePermission == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), PERMISSION_CODE
        )
    }

    // request permission
 /*   override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // on below line we are checking if the
        // request code is equal to permission code.
        if (requestCode == PERMISSION_CODE) {

            // on below line we are checking if result size is > 0
            if (grantResults.size > 0) {

                // on below line we are checking
                // if both the permissions are granted.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]
                    == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission accordée..", Toast.LENGTH_SHORT).show()

                    // if permissions are granted we are displaying a toast message.

                } else {

                    // if permissions are not granted we are
                    // displaying a toast message as permission denied.
                    Toast.makeText(this, "Permission Refusée..", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }
    }*/

    private fun getFileName(uri: Uri): String? {
        var name: String? = null
        if (uri.scheme.equals("content")) {
            contentResolver.query(uri, null, null, null, null)?.apply {
                moveToFirst()
                val columnIndex = getColumnIndex(OpenableColumns.DISPLAY_NAME)
                name = if (columnIndex >= 0) getString(columnIndex) else "unknown"
                close()
            }
        }
        if (name == null) {
            name = uri.path
            val cut = name?.lastIndexOf('/')
            if (cut != -1) {
                name = name?.substring(cut!! + 1)
            }
        }
        return name
    }


}

