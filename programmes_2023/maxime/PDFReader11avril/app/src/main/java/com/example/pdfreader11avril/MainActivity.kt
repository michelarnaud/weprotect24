package com.example.pdfreader11avril

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.barteksc.pdfviewer.PDFView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pdfview = findViewById<PDFView>(R.id.pdfview)
        pdfview.fromAsset("pdf1.pdf").enableSwipe(true).swipeHorizontal(false).load()


    }
}