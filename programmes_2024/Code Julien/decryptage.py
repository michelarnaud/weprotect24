import paho.mqtt.client as mqtt
import time
import base64
import json
import binascii
import requests

broker_address = "192.168.1.25"
broker_port = 1883
topic="application/e63fcd5b-2742-46f3-a642-3fffa9c9fba6/device/e24f43fffe44c6d6/event/up"
message_mqtt= None

def decoderData(message):
    try:
        # Convertir la chaîne JSON en un dictionnaire Python
        messageJSON = json.loads(message)

        # Récupérer la valeur associée à la clé "data"
        data = messageJSON.get("data", "")

        # Décoder la valeur en base64
        dataDecode = base64.b64decode(data)

        # Afficher le résultat
        hex_data = binascii.hexlify(dataDecode).decode('utf-8')
        return hex_data

    except json.JSONDecodeError as e:
        print("Erreur lors du décodage JSON :", str(e))
    except Exception as e:
        print("Une erreur s'est produite :", str(e))

# Fonction appelée lorsque la connexion au broker est établie
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(topic)

# Fonction appelée lorsqu'un message est reçu sur le topic abonné
def on_message(client, userdata, msg):
    global message_mqtt
    try:
        message_mqtt = msg.payload.decode('utf-8')
        print(f"Received message: {message_mqtt}")
    except UnicodeDecodeError:
        print("Failed to decode message with UTF-8 encoding. Trying 'latin-1' encoding.")
        message_mqtt = msg.payload.decode('latin-1')
        print(f"Received message: {message_mqtt}")
    message_mqtt=decoderData(message_mqtt)
    print("La data décodée est:",message_mqtt)
    
# Configuration du client MQTT
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

# Connexion au broker
client.connect(broker_address, broker_port, 60)

# Boucle principale pour maintenir la connexion et recevoir les messages
client.loop_forever()
